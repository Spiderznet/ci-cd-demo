import { NextFunction, Request, Response } from 'express'

const requestTimeMiddleware = (req: Request, res: Response, next: NextFunction) => {
    const startTime = new Date();
    res.on('finish', () => {
        const endTime = new Date();
        const diff = endTime.getTime() - startTime.getTime()
        // tslint:disable-next-line:no-console
        console.log(`${(diff)}ms`)
    })
    next()
}

export default requestTimeMiddleware