import { NextFunction, Request, Response, Router } from 'express'
import { createConnection } from 'mysql'
import dotenv from 'dotenv'

dotenv.config()

const DBRouter = Router()

interface ConnectionConfig {
    host: string,
    port: number,
    user: string,
    password: string,
    database: string,
}

DBRouter.get('/', (req: Request, res: Response, next: NextFunction) => {
    const mysqlHost = process.env.MYSQL_HOST || 'localhost';
    const mysqlPort = Number(process.env.MYSQL_PORT) || 3306;
    const mysqlUser = process.env.MYSQL_USER || 'root';
    const mysqlPass = process.env.MYSQL_PASS || '';
    const mysqlDB = process.env.MYSQL_DB || 'test';

    const connectionOptions: ConnectionConfig = {
        host: mysqlHost,
        port: mysqlPort,
        user: mysqlUser,
        password: mysqlPass,
        database: mysqlDB
    };

    const connection = createConnection(connectionOptions);
    const queryStr = `SELECT * FROM doe_users`;
    connection.connect();

    connection.query(queryStr, (error, results, fields) => {
        if (error) throw error;

        res.status(200).send(results);
    });

    connection.end();
})

export default DBRouter