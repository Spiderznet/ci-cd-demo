import express, { NextFunction, Request, Response } from 'express';
import dotenv from 'dotenv';
import requestTimeMiddleware from './middlewares/requestTimeMiddleware';
import DBRouter from './routes';
const app = express()
dotenv.config()

const PORT = process.env.PORT || 8200

app.use(requestTimeMiddleware)

app.use('/db', DBRouter)

app.get('/', (req: Request, res: Response, next: NextFunction) => {
    res.send({
        status: 1,
        message: 'Hello CI/CD!'
    })
})

app.get('/hello', (req: Request, res: Response, next: NextFunction) => {
    res.send({
        status: 1,
        message: 'Hello'
    })
})

app.listen(PORT, () => {
    // tslint:disable-next-line:no-console
    console.log(`App listing on http://localhost:${PORT}`)
})